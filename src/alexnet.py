import os.path
import sys
from skimage import io
import numpy as np

from keras.models import Sequential
from keras.layers import Dense, Activation
from keras.optimizers import SGD
from keras.layers.convolutional import Convolution2D
from keras.layers.pooling import MaxPooling2D
from keras.layers.core import Flatten, Dropout

def create_model(num_classes):
    model = Sequential()
    model.add(Convolution2D(64, 4, 4, border_mode='same', input_shape=(3, 64, 64), activation = "relu"))
    model.add(MaxPooling2D(pool_size=(2, 2), dim_ordering="th"))
    model.add(Convolution2D(256, 3, 3, border_mode='same', activation = "relu"))
    #model.add(MaxPooling2D(pool_size=(4, 4), dim_ordering="th"))
    model.add(Convolution2D(128, 3, 3, border_mode='same', activation = "relu"))
    #model.add(Convolution2D(256, 3, 3, border_mode='same', activation = "relu"))
    #model.add(Convolution2D(128, 3, 3, border_mode='same', activation = "relu"))

    model.add(Flatten())

    model.add(Dense(1024, activation="relu"))
    model.add(Dropout(0.5))
    model.add(Dense(1024, activation="relu"))
    model.add(Dropout(0.5))
    model.add(Dense(num_classes, activation="softmax"))

    sgd = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(loss='categorical_crossentropy',
              optimizer=sgd,
              metrics=['accuracy'])

    return model

def make_one_hot(labels, num_classes):
    one_hot_matrix = np.zeros(shape = (len(labels), num_classes))

    for idx, label in enumerate(labels):
        one_hot_matrix[idx, label] = 1

    return one_hot_matrix

def load_train_data(basedir, classes):
    basedir = os.path.expanduser(basedir)
    all_images = []
    labels = []
    backmap = {}
    if len(classes) == 0:
        classes = filter(lambda fname: os.path.isdir(os.path.join(basedir, "train", fname)), os.listdir((os.path.join(basedir, "train"))))
    for label, cls in enumerate(sorted(classes)):
        backmap[label] = cls
        
        train_dir_name = os.path.expanduser(os.path.join(basedir, "train", cls, "images"))
        for fname in os.listdir(train_dir_name):
            if fname.endswith("JPEG"):
                full_fpath = os.path.join(train_dir_name, fname)

                img = io.imread(full_fpath)
                if len(img.shape) == 2:
                    continue
                all_images.append(img)
                labels.append(label)

    images = np.array(all_images)
    #images = np.vstack(all_images)
    images = np.transpose(images, [0, 3, 2, 1])
    print("Loaded {} training samples".format(images.shape[0]))
    return images, make_one_hot(np.array(labels), len(classes)), len(classes), backmap

def load_validation_set(basedir, classes):
    basedir = os.path.expanduser(os.path.join(basedir, "val"))
    label_file_name = os.path.join(basedir, "val_annotations.txt")
    all_images = []
    labels = []

    fnames_per_class = {}

    with open(label_file_name) as f:
        for line in f:
            components = line.strip().split()
            fnames_per_class.setdefault(components[1], []).append(components[0])

    if len(classes) == 0:
        classes = list(fnames_per_class.keys())

    for label, cls in enumerate(sorted(classes)):
        for img_fname in fnames_per_class[cls]:
            image_path = os.path.expanduser(os.path.join(basedir, "images", img_fname))

            img = io.imread(image_path)
            if len(img.shape) == 2:
                continue
            all_images.append(img)
            labels.append(label)

    images = np.array(all_images)
    images = np.transpose(images, [0, 3, 2, 1])

    return images, make_one_hot(np.array(labels), len(classes)) 

if __name__ == "__main__":
    
    classes = []
    images, labels, cls_cnt, backmap = load_train_data("~/Downloads/tiny-imagenet-200", classes)

    with open("map.txt", "w") as f:
        for key, val in backmap.items():
            f.write(str(key) + "\t" + str(val) + "\n")

    model = create_model(cls_cnt)
    
    mean = np.mean(images)
    std = np.std(images)
    images = (images - mean) / std
    val_images, val_labels = load_validation_set("~/Downloads/tiny-imagenet-200", classes)
    val_images = (val_images - mean) / std
    
    model.fit(images, labels, nb_epoch=100, batch_size=1024)
    _, accuracy = model.evaluate(val_images, val_labels)
    print("Final accuracy", accuracy)

    model.save(sys.argv[1])
    
